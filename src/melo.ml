open Printf

type note_t = { time : float; id : int; time_on : int}
type keys_t = { keys : string; names : string array; freqs : int array;}
type melo_t = { keys : keys_t; start_time : float; scope : note_t array; pos : int}

let win_width = 1024
let win_height = 600

let enter_key = 13
let esc_key = 27
let back_key = 8
let space_key = 32
let b_key = 98
let c_key = 99
let d_key = 100
let f_key = 102
let k_key = 107
let m_key = 109
let p_key = 112


let nb_notes = 256
let initial_octave = 2
let nb_note_per_octave = 12
let basic_note = nb_note_per_octave * initial_octave  
let basic_freq = 22.5

let keyboard_filename = "melo.keyboard"
let melodies_filename = "melo.dies"

let foi = float_of_int

let round_to_int f =
  let fractional, integral = modf f in
  int_of_float(if fractional > 0.5 then integral +. 1. else integral)


let note_to_freq note =
  round_to_int(basic_freq *. (2. ** (foi (note + basic_note) /. (foi nb_note_per_octave))))


let init_notes_names_map() =
  let octave = if nb_note_per_octave = 12 then
      [|"A"; "A#"; "B"; "C"; "C#"; "D"; "D#"; "E"; "F"; "F#"; "G"; "G#"|]
    else Array.init nb_note_per_octave
        (fun i -> sprintf"%c" (char_of_int(int_of_char 'A' + i)))
  in
  Array.init nb_notes (fun i ->
      sprintf"%s%d" octave.(i mod nb_note_per_octave) (i / nb_note_per_octave + initial_octave))


let print_key key = printf"key %c = %d, " key (int_of_char key)


let save_keyboard keyboard =
  let oc = open_out keyboard_filename in

  fprintf oc "%s" keyboard;

  close_out oc;
  keyboard


let enter_keyboard() =

  Graphics.set_color Graphics.black;
  Graphics.fill_rect 0 0 win_width win_height;

  Graphics.moveto 20 (win_height - 30);

  Graphics.set_color Graphics.yellow;
  Graphics.draw_string "Enter keyboard from the lowest note to the highest note and hit enter.";

  let rec loop nb_keys keys =
    let event = Graphics.wait_next_event [Graphics.Key_pressed] in

    print_key event.key;

    let key = int_of_char event.key in

    if not event.keypressed || key = enter_key || nb_keys = nb_notes then
      (
        let keys_array = Array.of_list(List.rev keys) in
        let keyboard = String.init nb_keys (fun i -> keys_array.(i)) in
        save_keyboard keyboard
      )
    else
      loop (nb_keys + 1) (event.key::keys)
  in
  loop 0 []


let init_keyboard() =
  try
    let ic = open_in keyboard_filename in
    let keyboard = input_line ic in
    close_in ic;
    keyboard
  with _ -> enter_keyboard()


let init_keys() =
  let keys = init_keyboard() in
  let notes_names_map = init_notes_names_map() in

  let names = Array.make nb_notes "." in
  let freqs = Array.make nb_notes 0 in

  for i = 0 to String.length keys - 1 do
    let id = int_of_char keys.[i] in
    freqs.(id) <- note_to_freq i;
    names.(id) <- notes_names_map.(i);
  done;
  {keys; names; freqs}


let init_melo() =
  {keys = init_keys(); start_time = 0.; scope = [||]; pos = -1}


let print_note melo note =
  printf"note %d : freq = %d, time_on = %d, time = %f\n%!" note.id melo.keys.freqs.(note.id) note.time_on note.time


let print_notes melo pos =

  Graphics.set_color Graphics.black;
  Graphics.fill_rect 0 0 win_width win_height;

  let note_width = 30 in
  let notes_left = (win_width - Array.length melo.scope * note_width) / 2 in
  let notes_y = win_height / 2 in

  melo.scope |> Array.iteri(fun i note ->
      let x = notes_left + (i * note_width) in

      Graphics.moveto x notes_y;

      if i = pos then Graphics.set_color Graphics.magenta else Graphics.set_color Graphics.cyan;

      Graphics.draw_string melo.keys.names.(note.id);
    )


let play_note melo time note =

  let delay = note.time -. time in

  if delay > 0. then Unix.sleepf delay;

  let freq = melo.keys.freqs.(note.id) in

  if freq > 0 then Graphics.sound freq note.time_on


let playback melo pos =

  let rec loop start_time pos =
    let event = Graphics.wait_next_event [Graphics.Key_pressed; Poll] in

    if event.keypressed then (
      Graphics.wait_next_event [Graphics.Key_pressed] |> ignore
    )
    else (
      if pos = Array.length melo.scope then loop (Unix.gettimeofday()) 0
      else (
        play_note melo (Unix.gettimeofday() -. start_time) melo.scope.(pos);
        print_notes melo pos;
        loop start_time (pos + 1)
      )
    )
  in
  if Array.length melo.scope > 0 then loop (Unix.gettimeofday()) pos else ()


let save_melody melo =

  let oc = open_out_gen [Open_creat; Open_append] 0o666 melodies_filename in

  Array.iter(fun note -> fprintf oc " %.2f:%s" note.time melo.keys.names.(note.id)) melo.scope;

  output_string oc "\n";

  close_out oc


let rec run melo =

  print_notes melo melo.pos;

  let event = Graphics.wait_next_event [Graphics.Key_pressed] in

  if event.keypressed then (

    print_key event.key;

    let key = int_of_char event.key in

    if key = space_key then (
      playback melo melo.pos;
      run melo;
    )
    else if key = b_key && melo.pos > 0 then run {melo with pos = melo.pos - 1}
    else if key = b_key then run {melo with pos = Array.length melo.scope - 1}
    else if key = f_key && melo.pos >= Array.length melo.scope - 1 then run {melo with pos = 0}
    else if key = f_key then run {melo with pos = melo.pos + 1}

    else if key = back_key then (
      let nb_notes = Array.length melo.scope - 1 in
      printf"nb_notes %d, melo.pos %d\n%!" nb_notes melo.pos;

      if nb_notes < 0 then run melo
      else
      if melo.pos = 0 then run {melo with scope = Array.sub melo.scope 1 nb_notes; pos = 0}
      else
      if melo.pos = nb_notes then run {melo with scope = Array.sub melo.scope 0 nb_notes; pos = melo.pos - 1}
      else
        run {melo with scope = Array.(append(sub melo.scope 0 melo.pos) (sub melo.scope (melo.pos + 1) (nb_notes - melo.pos))); pos = melo.pos - 1}
    )
    else if key = d_key then run {melo with start_time = 0.; scope = [||]; pos = -1}
    else if key < 0 || key >= nb_notes || key = esc_key then ()
    else if key = k_key then run {melo with keys = init_keys()}
    else (
      let time = Unix.gettimeofday() in
      let start_time = if melo.scope = [||] then time else melo.start_time in
      let note = {time = time -. start_time; id = key; time_on = 125} in
      let melo = {melo with start_time; scope = Array.append melo.scope [|note|]; pos = melo.pos + 1} in

      if key = enter_key then save_melody melo
      else play_note melo note.time note;

      run melo
    )
  )
  else run melo


let () =
  Graphics.open_graph (" " ^ string_of_int win_width ^ "x" ^ string_of_int win_height);

  init_melo() |> run
