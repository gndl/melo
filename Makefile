.PHONY: default build install uninstall test clean

default: build

dep:
	opam install -y --deps-only .

build:
	dune build src/melo.exe

dbg:
	dune build examples/test.bc
	cp ./_build/default/examples/test.bc .

test:
	dune runtest -f
	dune exec test/test.exe

exec:
	dune exec src/melo.exe

install:
	dune install

uninstall:
	dune uninstall

clean:
	dune clean
# Optionally, remove all files/folders ignored by git as defined
# in .gitignore (-X).
#git clean -dfXq
